options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    catch_exceptions: 'True'
    category: Custom
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: A CW (Morse) Decoder
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: satnogs_cw_decoder
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: CW Decoder
    window_size: 3000, 3000
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1504, 12.0]
    rotation: 0
    state: enabled
- name: dot_samples
  id: variable
  parameters:
    comment: ''
    value: int((1.2 / wpm) / (1.0 / (audio_samp_rate / 10.0)))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 12.0]
    rotation: 0
    state: enabled
- name: variable_cw_decoder_0
  id: variable_cw_decoder
  parameters:
    channels: '4'
    comment: ''
    confidence: '0.90'
    fft_size: '512'
    max_frame_size: '96'
    min_frame_size: '8'
    overlapping: 512-8
    samp_rate: audio_samp_rate/4
    snr: '10'
    wpm: wpm
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1328, 372.0]
    rotation: 0
    state: true
- name: analog_agc2_xx_0_0
  id: analog_agc2_xx
  parameters:
    affinity: ''
    alias: ''
    attack_rate: '0.01'
    comment: ''
    decay_rate: '0.001'
    gain: '0.0'
    max_gain: '65536'
    maxoutbuf: '0'
    minoutbuf: '0'
    reference: '0.015'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 420.0]
    rotation: 0
    state: enabled
- name: antenna
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [880, 12.0]
    rotation: 0
    state: enabled
- name: bb_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Baseband CORDIC frequency (if the device supports it)
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1928, 156.0]
    rotation: 0
    state: enabled
- name: bfo_freq
  id: parameter
  parameters:
    alias: ''
    comment: 'The bfo_freq shifts the carrier from 0 Hz to a

      frequency that corresponds to the CW audio

      tone. This tone is typically 1 kHz.'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 1e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1728, 12.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_interleaved_short_0_0
  id: blocks_complex_to_interleaved_short
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    scale_factor: '1.0'
    vector_output: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [112, 324.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_real_0
  id: blocks_complex_to_real
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1000, 456.0]
    rotation: 0
    state: enabled
- name: blocks_file_sink_0_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: iq_file_path
    type: short
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 348.0]
    rotation: 0
    state: enabled
- name: blocks_null_sink_0
  id: blocks_null_sink
  parameters:
    affinity: ''
    alias: ''
    bus_structure_sink: '[[0,],]'
    comment: ''
    num_inputs: '1'
    type: short
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 320.0]
    rotation: 0
    state: enabled
- name: blocks_rotator_cc_0_0
  id: blocks_rotator_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    phase_inc: 2.0 * math.pi * (bfo_freq / audio_samp_rate)
    tag_inc_update: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 452.0]
    rotation: 0
    state: enabled
- name: blocks_selector_0_0
  id: blocks_selector
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    enabled: 'True'
    input_index: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '1'
    num_outputs: '2'
    output_index: enable_iq_dump
    showports: 'True'
    type: short
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [336, 300.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1232, 12.0]
    rotation: 0
    state: enabled
- name: dc_removal
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Remove automatically the DC offset (if the device support it)
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1776, 156.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/.satnogs/data/data"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 820.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [432, 12.0]
    rotation: 0
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [160, 820.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [256, 740.0]
    rotation: 0
    state: enabled
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"test.wav"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 740.0]
    rotation: 180
    state: enabled
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: "Generic gain value. \nEach Soapy module\nwill try to linearize this\n\
      value by properly setting\nthe device specific gains."
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1104, 12.0]
    rotation: 0
    state: enabled
- name: gain_mode
  id: parameter
  parameters:
    alias: ''
    comment: 'Set the gain mode of the Soapy.

      Can be "Overall", "Specific"

      or "Settings Field"'
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"Overall"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [968, 12.0]
    rotation: 0
    state: enabled
- name: import_1
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 68.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/iq.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 740.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 12.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: '3000'
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: audio_samp_rate
    type: fir_filter_ccf
    width: 1e3
    win: window.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [392, 516.0]
    rotation: 0
    state: enabled
- name: network_udp_sink_0_0
  id: network_udp_sink
  parameters:
    addr: udp_dump_host
    affinity: ''
    alias: ''
    comment: ''
    header: '0'
    payloadsize: '1472'
    port: udp_dump_port
    send_eof: 'False'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [976, 884.0]
    rotation: 0
    state: enabled
- name: other_settings
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Channel other settings
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 156.0]
    rotation: 0
    state: enabled
- name: ppm
  id: parameter
  parameters:
    alias: ''
    comment: 'The frequency correction in PPM

      to correct for a local oscillator frequency deviation'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1952, 4.0]
    rotation: 0
    state: enabled
- name: rational_resampler_xxx_0
  id: rational_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: '4'
    fbw: '0'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    taps: ''
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 540.0]
    rotation: 0
    state: true
- name: rigctl_host
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 900.0]
    rotation: 0
    state: enabled
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 820.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 12.0]
    rotation: 0
    state: enabled
- name: samp_rate_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 12.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: audio_samp_rate
    samp_rate: samp_rate_rx
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [608, 164.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_cw_decoder_0
    itype: complex
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 568.0]
    rotation: 0
    state: true
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1048, 596.0]
    rotation: 0
    state: enabled
- name: satnogs_ogg_encoder_0
  id: satnogs_ogg_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filename: file_path
    quality: '1.0'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1144, 436.0]
    rotation: 0
    state: enabled
- name: satnogs_waterfall_sink_0
  id: satnogs_waterfall_sink
  parameters:
    affinity: ''
    alias: ''
    center_freq: rx_freq
    comment: ''
    fft_size: '1024'
    filename: waterfall_file_path
    mode: '1'
    rps: '10'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [48, 604.0]
    rotation: 180
    state: enabled
- name: snippet_0
  id: snippet
  parameters:
    alias: ''
    code: self.satnogs_doppler_compensation_0.variable_rigctl.set_param("rig_pathname",
      str(self.rigctl_host)+ ":"+str(self.rigctl_port) )
    comment: Convenient way to support arbitrary rigctl sources
    priority: '0'
    section: main_after_init
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 124.0]
    rotation: 0
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"driver=invalid"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [304, 12.0]
    rotation: 0
    state: enabled
- name: soapy_source_0_0
  id: soapy_source
  parameters:
    affinity: ''
    agc0: 'False'
    agc1: 'False'
    alias: ''
    amp_gain0: '0'
    ant0: antenna
    ant1: RX2
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: ppm
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_removal0: dc_removal
    dc_removal1: 'True'
    dev: soapy_rx_device
    dev_args: ''
    devname: custom
    gain_mode0: gain_mode
    gain_mode1: Overall
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    nchan: '1'
    nco_freq0: bb_freq
    nco_freq1: '0'
    overall_gain0: gain
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rf_gain0: '18'
    rfgr_gain: '9'
    rxvga1_gain: '5'
    rxvga2_gain: '0'
    samp_rate: samp_rate_rx
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    settings0: other_settings
    settings1: ''
    stream_args: stream_args
    tia_gain0: '0'
    tia_gain1: '0'
    tune_args0: tune_args
    tune_args1: ''
    tuner_gain0: '10'
    type: fc32
    vga_gain0: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [312, 132.0]
    rotation: 0
    state: true
- name: stream_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Stream arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1288, 156.0]
    rotation: 0
    state: enabled
- name: tune_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Channel Tune arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1464, 156.0]
    rotation: 0
    state: enabled
- name: udp_dump_host
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [736, 772.0]
    rotation: 0
    state: enabled
- name: udp_dump_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '57356'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [864, 772.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [896, 196.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 540.0]
    rotation: 0
    state: true
- name: virtual_source_0_0_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [600, 916.0]
    rotation: 0
    state: enabled
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/waterfall.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 740.0]
    rotation: 0
    state: enabled
- name: wpm
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1416, 12.0]
    rotation: 0
    state: enabled
- name: zeromq_pub_msg_sink_1_0
  id: zeromq_pub_msg_sink
  parameters:
    address: zmq_pub_uri
    affinity: ''
    alias: ''
    bind: 'True'
    comment: ''
    timeout: '100'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1256, 596.0]
    rotation: 0
    state: enabled
- name: zmq_pub_uri
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: tcp://127.0.0.1:16887
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1256, 676.0]
    rotation: 0
    state: enabled

connections:
- [analog_agc2_xx_0_0, '0', blocks_rotator_cc_0_0, '0']
- [blocks_complex_to_interleaved_short_0_0, '0', blocks_selector_0_0, '0']
- [blocks_complex_to_real_0, '0', satnogs_ogg_encoder_0, '0']
- [blocks_rotator_cc_0_0, '0', blocks_complex_to_real_0, '0']
- [blocks_selector_0_0, '0', blocks_null_sink_0, '0']
- [blocks_selector_0_0, '1', blocks_file_sink_0_0, '0']
- [low_pass_filter_0_0, '0', analog_agc2_xx_0_0, '0']
- [low_pass_filter_0_0, '0', rational_resampler_xxx_0, '0']
- [rational_resampler_xxx_0, '0', satnogs_frame_decoder_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0, out, satnogs_json_converter_0, in]
- [satnogs_json_converter_0, out, zeromq_pub_msg_sink_1_0, in]
- [soapy_source_0_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', blocks_complex_to_interleaved_short_0_0, '0']
- [virtual_source_0, '0', low_pass_filter_0_0, '0']
- [virtual_source_0, '0', satnogs_waterfall_sink_0, '0']
- [virtual_source_0_0_0, '0', network_udp_sink_0_0, '0']

metadata:
  file_format: 1
